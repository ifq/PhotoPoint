package gui

import (
	//"fmt"
	"github.com/mattn/go-gtk/glib"
	"github.com/mattn/go-gtk/gtk"
	"github.com/mattn/go-gtk/gdkpixbuf"
	"os"
)

const (
	ICON_PATH = "static/img/icon.png"
)

func TrayIconInit() {

	gtk.Init(&os.Args)

	glib.SetApplicationName("go-gtk-statusicon-example")

	mi := gtk.NewMenuItemWithLabel("Exit")
	mi.Connect("activate", func() {
		gtk.MainQuit()
		os.Exit(0)
	})
	nm := gtk.NewMenu()
	nm.Append(mi)
	nm.ShowAll()

	var si *gtk.StatusIcon
	icon, err := gdkpixbuf.NewFromFile(ICON_PATH)
	if err != nil {
		si = gtk.NewStatusIconFromStock(gtk.STOCK_FILE)
	} else {
		si = gtk.NewStatusIconFromPixbuf(icon)
	}
	si.SetTitle("StatusIcon Example")
	si.SetTooltipMarkup("StatusIcon Example")
	si.Connect("popup-menu", func(cbx *glib.CallbackContext) {
		nm.Popup(nil, nil, gtk.StatusIconPositionMenu, si, uint(cbx.Args(0)), uint32(cbx.Args(1)))
	})

	gtk.Main()
}











