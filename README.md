# PhotoPoint
## 概述

![photopoint logo](http://git.oschina.net/sleepywk/PhotoPoint/raw/master/logo.jpg)

**Photo Point**主要使用go语言实现。通过在的存储照片的计算机上运行临时的web server，从而在局域网内的共享照片，适合在家庭内使用。用户通过手机或者平板上的浏览器即可访问计算机上的图片。

现在数码相机普及，每家人都有很多照片存储在电脑中。但很少有机会围坐在电脑前一起欣赏图片。 

通过photo point可能增进照片欣赏的体验。比如，家里的男女老少可以在自己的手机上随时欣赏照片，朋友来串门可以用ipad展示图片。未来，可以增加TAG功能、地理位置分类、时间线、最近浏览、我的最爱、主题等浏览功能，增强使用体验。

photo point安装简单，绿色安装，不需要在本地架设复杂的web server。程序点开运行即开启浏览功能。

**“Photo Point”**名称意义：photo的access point、hot spot。

使用go语言实现，可以跨平台编译运行。目前尝试在linux和win下编译运行。

这是我去年夏天和朋友做的一个demo，后来没有时间继续了就沉睡在这里了。现在重新打开，看看能不能给它注入点新鲜血液。

## 预览
http://pan.baidu.com/s/1i3CDJaX
- 可以通过这个链接下载win32环境下的demo程序，不明白的可以参考demo-usage.docx或者在网站上提问。
- 该程序需要以来 GTK+， 可以在这里[下载](http://pan.baidu.com/s/1eQpdrqy)

![demo1](http://git.oschina.net/ifq/PhotoPoint/attach_files/download?i=6945&u=http%3A%2F%2Ffiles.git.oschina.net%2Fgroup1%2FM00%2F00%2F46%2FcHwGbFR8Q-2AR-1dAABI_pn5UVA019.png%3Ftoken%3D7b810b0f68c32eba152fcfbd49fe7007%26ts%3D1417430026%26filename%3Dpp2.png)
![demo2](http://git.oschina.net/ifq/PhotoPoint/attach_files/download?i=6946&u=http%3A%2F%2Ffiles.git.oschina.net%2Fgroup1%2FM00%2F00%2F46%2FcHwGbFR8Q_qAM9dDAAJvYVfPECg625.png%3Ftoken%3D804bc23613388351158ff517a152a3bc%26ts%3D1417430026%26filename%3Dpp1.png)



## TODO ##

### 后端 ###

* 缩略图的速度优化
* 对很大的图片生成小图，加速浏览
* 修改 Imgobj 支持缩略图
* 中文文件夹名支持？
* GUI

### 前端 ###

* 界面完善
* 首页



