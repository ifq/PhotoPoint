package controllers

import (
	"github.com/astaxie/beego"
)

type MainController struct {
	beego.Controller
}

func (this *MainController) Get() {
	this.Data["Website"] = "PhotoPoint"
	this.Data["Email"] = "ifqqfi@gmail.com"
	this.TplNames = "index.tpl"
}
