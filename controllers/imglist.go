package controllers

import (
	"github.com/astaxie/beego"
	"photopoint/models"
	"fmt"
	"log"
)

const IMG_TMPL = "<li><a href=\"%s\"><img src=\"%s\" alt=\"%s\" /></a></li>\n"

type ImgListController struct {
	beego.Controller
}

func (this *ImgListController) Get() {
	
	id, err := this.GetInt("dir")
	if err != nil {
		log.Println("imglist get err.")
		return
	}
	log.Println(id)
	
	dirobj, _ := models.DbGetFolder(uint64(id))
	imgcontent := ""
	if dirobj != nil {
		log.Println(len(dirobj.Imgs))
		for _, img := range dirobj.Imgs {
			name := models.TrimRootPath(dirobj.Path + img.Name)
			tpath := "/preview/" + name
			path := "/rc/" + name

			log.Println(path, img.Size)
			imgcontent += fmt.Sprintf(IMG_TMPL, path, tpath, img.Time)

		}
	}

	log.Println(imgcontent)
	this.Data["imglst"] = imgcontent
	this.Data["Website"] = "PhotoPoint"
	this.Data["Email"] = "ifqqfi@gmail.com"
	this.TplNames = "img_list.html"
}








