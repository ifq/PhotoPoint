
package controllers

import (
	"github.com/astaxie/beego"
	"fmt"
	"photopoint/models"
	"strconv"
	"log"
)

const FOLDER_TMPL = `		<li>
			<a href="%s">
				<img src="%s" alt="%s" />
				<p>%s</p>
			</a>
		</li>
`

type FolderController struct {
	beego.Controller
}

func (this *FolderController) Get() {
	folderstr := ""
	dirs, _ := models.DbGetAllFolders()
	for id, dirobj := range dirs {
		path := dirobj.Path
		if  dirobj.Imgs == nil || len(dirobj.Imgs) <= 0 {
			continue
		}
		imgobj := dirobj.Imgs[0]
		log.Println("get dbg:", path, imgobj.Name)
		imgpath := path + imgobj.Name
		pageurl := "/imgs?dir=" + strconv.FormatUint(id, 10)
		thumb := "/preview/" + models.TrimRootPath(imgpath)
		
		folderstr += fmt.Sprintf(FOLDER_TMPL, pageurl, thumb, imgobj.Time,imgobj.Time)
	}

	this.Data["folderlst"] = folderstr
	this.Data["Website"] = "PhotoPoint"
	this.Data["Email"] = "ifqqfi@gmail.com"
	this.TplNames = "folder.html"
}











