package models

import (
	"log"
)

func InitAll(root string) {

	ResizeInit()
	
	dirs, _ := ScanRoot(root)
	PrintDirs(dirs)

	InitDb(root)
	SyncDb(dirs)

	result, _ := DbGetAllFolders()
	for id, item := range result {
		log.Println(id, item)
	}
}



















