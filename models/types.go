package models

import (
	"time"
	// "fmt"
	// "log"
	// "runtime"
	// "path/filepath"
	"os"
	// "strings"
	// "image/jpeg"
	// "github.com/nfnt/resize"
	"image"
)

const (
	PPROOT		= ".photopoint"
	THUMB_DIR	= PPROOT + SPT + "thumb" + SPT
	PREVIEW_DIR = PPROOT + SPT + "preview" + SPT
	DB_DIR		= PPROOT + SPT + "db" + SPT
	SPT         = string(os.PathSeparator)
)

const (
	DB_FOLDERS  = "FOLDERS"

)

type DirStats int
const (
	UNKNOWN DirStats = iota
	SYNC
	MISS
)

type Imgdir struct {
	Path string
	Cnt  int
	Imgs []*Imgobj
	Hash uint64
	Stats DirStats
}

type Imgobj struct {
	Name		 string
	Size		 int64
	Time		 time.Time
	Tags		 []string
	Location	 string
	Thumb		 Imgrsz
	Preview		 Imgrsz
}

type RszStats int
const (
	None RszStats = iota
	Done
	Doing
)

type Imgrsz struct {
	Path string
	Size image.Point
 	Stats RszStats
}




















