package main

import (
	"photopoint/controllers"
	"photopoint/gui"
	"photopoint/models"
	"github.com/astaxie/beego"
	"flag"
	"log"
	"path/filepath"
	"fmt"
)


func main() {

	go gui.TrayIconInit()
	
	fmt.Println("initializing...")
	flag.Parse()
	root := filepath.Dir(flag.Arg(0)) + models.SPT

	models.InitAll(root)
	
	fmt.Println("good to go...")

	beego.Router("/", &controllers.FolderController{})
	beego.Router("/imgs", &controllers.ImgListController{})

	log.Println(root)
	beego.SetStaticPath("/rc", root)
	beego.SetStaticPath("/thumb", models.ThumbPath)
	beego.SetStaticPath("/preview", models.PreviewPath)
	beego.SetStaticPath("/image","static/img")
	beego.SetStaticPath("/css","static/css")
	beego.SetStaticPath("/js","static/js")

	beego.Run()
}







